<?php
/*
 Template Name: Courses Page
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php 
						$term = get_field('quarter');
						$qt = $term->name;
					?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
					</article>
										
					<?php // Languages
					$language_loop = new WP_Query( 
						array( 'quarter' => "'$qt'", 'post_type' => 'courses', 'posts_per_page' => -1, 'orderby' => 'meta_value_num', 'meta_key' => 'course_number', 'order' => 'ASC', 'meta_query' =>
						array(
							array(
								'key' => 'program',
								'value' => 'language',
							))
						));
					?>
					<h2><?php echo $qt; ?>: Language Courses</h2>
					<?php if ( $language_loop->have_posts() ) : while ( $language_loop->have_posts() ) : $language_loop->the_post(); ?>
					<div class="course">
						<h4><?php the_title(); ?></h4>
						<?php if(get_field('instructor_type') == "internal") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<?php $language_loop->reset_postdata(); ?>
							<?php endif; ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "external") { ?>
						<span class="instructors">
							<?php if(get_field('additional_instructors')) { ?>
							<strong>Instructor: </strong><?php the_field('additional_instructors'); ?>
							<?php } ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "both") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php $language_loop->reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_instructors')) { ?>, <?php the_field('additional_instructors'); } ?>
						</span>
						<?php }	?>
						<?php the_content(); ?>
					</div>
					<?php endwhile; else : ?>
					<p>There are no language courses this quarter.</p>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
					
					<?php // Languages
					$english_loop = new WP_Query( 
						array( 'quarter' => "'$qt'", 'post_type' => 'courses', 'posts_per_page' => -1, 'orderby' => 'meta_value_num', 'meta_key' => 'course_number', 'order' => 'ASC', 'meta_query' => 
						array(
							array(
								'key' => 'program',
								'value' => 'english',
							))
						));
					?>
					<h2><?php echo $qt; ?>: Courses in English</h2>
					<?php if ( $english_loop->have_posts() ) : while ( $english_loop->have_posts() ) : $english_loop->the_post(); ?>
					<div class="course">
						<h4><?php the_title(); ?></h4>
						<?php if(get_field('instructor_type') == "internal") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<?php $english_loop->reset_postdata(); ?>
							<?php endif; ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "external") { ?>
						<span class="instructors">
							<?php if(get_field('additional_instructors')) { ?>
							<strong>Instructor: </strong><?php the_field('additional_instructors'); ?>
							<?php } ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "both") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php $english_loop->reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_instructors')) { ?>, <?php the_field('additional_instructors'); } ?>
						</span>
						<?php }	?>
						<?php the_content(); ?>
					</div>
					<?php endwhile; else : ?>
					<p>There are no courses in English this quarter.</p>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
					
					<?php // Languages
					$italian_loop = new WP_Query( 
						array( 'quarter' => "'$qt'", 'post_type' => 'courses', 'posts_per_page' => -1, 'orderby' => 'meta_value_num', 'meta_key' => 'course_number', 'order' => 'ASC', 'meta_query' =>
						array(
							array(
								'key' => 'program',
								'value' => 'italian',
							))
						));
					?>
					<h2><?php echo $qt; ?>: Courses in Italian</h2>
					<?php if ( $italian_loop->have_posts() ) : while ( $italian_loop->have_posts() ) : $italian_loop->the_post(); ?>
					<div class="course">
						<h4><?php the_title(); ?></h4>
						<?php if(get_field('instructor_type') == "internal") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<?php $italian_loop->reset_postdata(); ?>
							<?php endif; ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "external") { ?>
						<span class="instructors">
							<?php if(get_field('additional_instructors')) { ?>
							<strong>Instructor: </strong><?php the_field('additional_instructors'); ?>
							<?php } ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "both") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php $italian_loop->reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_instructors')) { ?>, <?php the_field('additional_instructors'); } ?>
						</span>
						<?php }	?>
						<?php the_content(); ?>
					</div>
					<?php endwhile; else : ?>
					<p>There are no courses in Italian this quarter.</p>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
					
					<?php // Languages
					$graduate_loop = new WP_Query( 
						array( 'quarter' => "'$qt'", 'post_type' => 'courses', 'posts_per_page' => -1, 'orderby' => 'meta_value_num', 'meta_key' => 'course_number', 'order' => 'ASC', 'meta_query' =>
						array(
							array(
								'key' => 'program',
								'value' => 'graduate',
							))
						));
					?>
					<h2><?php echo $qt; ?>: Graduate Courses</h2>
					<?php if ( $graduate_loop->have_posts() ) : while ( $graduate_loop->have_posts() ) : $graduate_loop->the_post(); ?>
					<div class="course">
						<h4><?php the_title(); ?></h4>
						<?php if(get_field('instructor_type') == "internal") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<?php $graduate_loop->reset_postdata(); ?>
							<?php endif; ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "external") { ?>
						<span class="instructors">
							<?php if(get_field('additional_instructors')) { ?>
							<strong>Instructor: </strong><?php the_field('additional_instructors'); ?>
							<?php } ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "both") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php $graduate_loop->reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_instructors')) { ?>, <?php the_field('additional_instructors'); } ?>
						</span>
						<?php }	?>
						<?php the_content(); ?>
					</div>
					<?php endwhile; else : ?>
					<p>There are no graduate courses this quarter.</p>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
			<?php endwhile; else : ?>
			<?php endif; ?>
<?php get_footer(); ?>
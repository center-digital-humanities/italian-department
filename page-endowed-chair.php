<?php
/*
 Template Name: Charles Speroni Chair Page
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_post_thumbnail('content-width'); ?>
							<?php
							$charles_speroni_chair = get_field('charles_speroni_chair');
							if( $charles_speroni_chair ): 
								// override $post
								$post = $charles_speroni_chair;
								setup_postdata( $post ); 
							?>
							<div class="people-list">
								<div class="person-item">
									<a href="<?php the_permalink() ?>" class="photo-link">
										<?php // if there is a photo, use it
										if(get_field('photo')) {
											$image = get_field('photo');
											if( !empty($image) ): 
												// vars
												$url = $image['url'];
												$title = $image['title'];
												// thumbnail
												$size = 'people-thumb';
												$thumb = $image['sizes'][ $size ];
												$width = $image['sizes'][ $size . '-width' ];
												$height = $image['sizes'][ $size . '-height' ];
										endif; ?>
										<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
										<?php // otherwise use a silhouette
										} else { ?>
										<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
										<?php } ?>
									</a>
									<div class="about">
										<h3 class="name"><?php the_title(); ?></h3>
										<ul class="details">
											<?php if(get_field('position_title')) { ?>
											<li class="position"><?php the_field('position_title'); ?></li>
											<?php } ?>
											<?php if(get_field('email_address')) { ?>
											<li class="email"><strong>E-mail: </strong><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a></li>
											<?php } ?>
											<?php if(get_field('phone_number')) { ?>
											<li class="phone"><strong>Phone: </strong><?php the_field('phone_number'); ?></li>
											<?php } ?>
											<?php if(get_field('office')) { ?>
											<li class="office"><strong>Office: </strong><?php the_field('office'); ?></li>
											<?php } ?>
											<?php if(get_field('office_hours')) { ?>
											<li class="hours"><strong>Office Hours: </strong><?php the_field('office_hours'); ?></li>
											<?php } ?>
											<?php if(get_field('education')) { ?>
											<li class="education"><?php the_field('education'); ?></li>
											<?php } 
											?>
											<?php if(get_field('personal_website')) { ?>
											<li><a href="<?php the_field('personal_website'); ?>" class="link">Personal Website</a></li>
											<?php } ?>
										</ul>
										<p>
											<?php $content = get_the_content();
											$trimmed_content = wp_trim_words( $content, 40, '...' );
											echo $trimmed_content; ?> <a class="view-all" href="<?php the_permalink() ?>">&raquo; Read More<span class="hidden"> About <?php the_title(); ?></span></a>
										</p>
									</div>
								</div>
							</div>
							<?php wp_reset_postdata(); ?>
							<?php endif; ?>
							<?php the_content(); ?>
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>
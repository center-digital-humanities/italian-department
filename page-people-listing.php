<?php
/*
 Template Name: People Listing
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<header>
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
						<?php // Select what people category to show
						$people_category = get_field('people_category');
						if( $people_category ) {
							$people_cat = $people_category->slug;
						}
						// Set varaibles to decide behavior of page
						if ( get_field('link_to_pages') == 'yes' ) {
							$person_link = 'yes';
						}
						$people_details = get_field('people_details');
						if( in_array('position', $people_details) ) { 
							$position = 'yes';
						} 
						if( in_array('education', $people_details) ) {
							$education = 'yes';
						} 
						if( in_array('email', $people_details) ) {
							$email = 'yes';
						}
						if( in_array('phone', $people_details) ) {
							$phone = 'yes';
						} 
						if( in_array('office', $people_details) ) {
							$office = 'yes';
						} 
						if( in_array('hours', $people_details) ) {
							$hours = 'yes';
						} 
						?>
					</header>
					<div class="people-list">
						<ul class="<?php echo $people_cat ?>">
						<?php $core_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC')); ?>
						<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
							<li class="person-item">
								<?php if ( $person_link == 'yes' ) { ?>
								<a href="<?php the_permalink() ?>" class="photo-link">
								<?php } ?>
									<?php // if there is a photo, use it
									if(get_field('photo')) {
										$image = get_field('photo');
										if( !empty($image) ): 
											// vars
											$url = $image['url'];
											$title = $image['title'];
											// thumbnail
											$size = 'people-thumb';
											$thumb = $image['sizes'][ $size ];
											$width = $image['sizes'][ $size . '-width' ];
											$height = $image['sizes'][ $size . '-height' ];
									endif; ?>
									<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php // otherwise use a silhouette
									} else { ?>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php } ?>
								<?php if ( $person_link == 'yes' ) { ?>
								</a>
								<?php } ?>
								<div class="about">
									<?php if ( $person_link == 'yes' ) { ?>
									<a href="<?php the_permalink() ?>">
									<?php } ?>
										<h3 class="name"><?php the_title(); ?></h3>
									<?php if ( $person_link == 'yes' ) { ?>
									</a>
									<?php } ?>
									<ul class="details">
										<?php if ( $position == 'yes' ) { ?>
										<?php if(get_field('position_title')) { ?>
										<li class="position"><?php the_field('position_title'); ?></li>
										<?php } ?>
										<?php } 
										if ( $email == 'yes' ) { ?>
										<?php if(get_field('email_address')) { ?>
										<li class="email"><strong>E-mail: </strong><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a></li>
										<?php } ?>
										<?php }
										if ( $phone == 'yes' ) { ?>
										<?php if(get_field('phone_number')) { ?>
										<li class="phone"><strong>Phone: </strong><a href="tel:<?php the_sub_field('phone_number'); ?>"><?php the_field('phone_number'); ?></a></li>
										<?php } ?>
										<?php } 
										if ( $office == 'yes' ) { ?>
										<?php if(get_field('office')) { ?>
										<li class="office"><strong>Office: </strong><?php the_field('office'); ?></li>
										<?php } ?>
										<?php }
										if ( $hours == 'yes' ) { ?>
										<?php if(get_field('office_hours')) { ?>
										<li class="hours"><strong>Office Hours: </strong><?php the_field('office_hours'); ?></li>
										<?php } ?>
										<?php }
										if ( $education == 'yes' ) { ?>
										<?php if(get_field('education')) { ?>
										<li class="education"><?php the_field('education'); ?></li>
										<?php } ?>
										<?php } 
										?>
									</ul>
									<?php if( empty( $post->post_content) ) {
									// If there is no bio, don't show bio link
									} else { ?>
									<p>
										<?php $content = get_the_content();
										$trimmed_content = wp_trim_words( $content, 40, '...' );
										echo $trimmed_content; ?> <a class="view-all" href="<?php the_permalink() ?>">&raquo; Read More<span class="hidden"> About <?php the_title(); ?></span></a>
									</p>
									<?php } ?>
								</div>
							</li>
						<?php endwhile; ?>					
						</ul>					
					</div>
				</div>
				<?php get_sidebar(); ?>
			</div>
<?php get_footer(); ?>
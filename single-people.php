<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">
						<header>
							<?php if(get_field('photo')) {
								$image = get_field('photo');
								if( !empty($image) ): 
								// vars
								$url = $image['url'];
								$title = $image['title'];
								// thumbnail
								$size = 'people-large';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ];
							endif; ?>
							<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
							<?php } else { ?>
							<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="Silhouette" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
							<?php } ?>
							<div class="about">
								<h1 id="bio"><?php the_title(); ?></h1>
								<?php if(get_field('position_title')) { ?>
								<strong class="position"><?php the_field('position_title'); ?></strong>
								<?php } ?>
								<ul class="details">
								<?php if(get_field('email_address')) { ?>
									<li class="email"><?php $person_email = antispambot(get_field('email_address')); ?>
									<span><strong>E-mail:</strong> <a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a></span></li>
								<?php } ?>
								<?php if(get_field('phone_number')) { ?>
									<li class="phone"><strong>Phone: </strong><a href="tel:<?php the_sub_field('phone_number'); ?>"><?php the_field('phone_number'); ?></a></li>
								<?php } ?>
								<?php if(get_field('office')) { ?>
									<li class="office"><strong>Office: </strong><?php the_field('office'); ?></li>
								<?php } ?>
								<?php if(get_field('office_hours')) { ?>
									<li class="hours"><strong>Office Hours: </strong><?php the_field('office_hours'); ?></li>
								<?php } ?>
								<?php if(get_field('education')) { ?>
									<li class="education"><?php the_field('education'); ?></li>
								<?php } ?>
								</ul>
								<?php 
								if(get_field('cv') || get_field('personal_website') || get_field('academia_profile') || get_field('additional_link')) { ?>
								<ul class="additional-links">
								<?php if(get_field('cv')) { ?>
									<li><a href="<?php the_field('cv'); ?>" class="download"><span class="fas fa-download"></span>Download CV</a></li>
								<?php } ?>
								<?php if(get_field('personal_website')) { ?>
									<li><a href="<?php the_field('personal_website'); ?>" class="link"><span class="fas fa-link"></span>Personal Website</a></li>
								<?php } ?>
								<?php if(get_field('academia_profile')) { ?>
									<li><a href="<?php the_field('academia_profile'); ?>" class="link"><span class="fas fa-link"></span>Academia Profile</a></li>
								<?php } ?>
								<?php if(get_field('additional_link')) { ?>
									<li><a href="<?php the_field('additional_link'); ?>" class="link"><span class="fas fa-link"></span><?php the_field('additional_link_title'); ?></a></li>
								<?php } ?>
								</ul>
								<?php } ?>
							</div>
						</header>
						<section class="bio">
							<?php the_content(); ?>
						</section>
						<?php if(get_field('courses')) { ?>
						<section id="courses">
							<h2>Courses Taught</h2>
							<?php the_field('courses'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('books')) { ?>
						<section id="books">
							<h2>Books</h2>
							<?php $book = get_field('books'); ?>
							<ul class="book-list">
								<? if( $book ): ?>
								<?php foreach( $book as $post): ?>
								<?php setup_postdata($post); ?>
								<li>
									<?php if(get_field('book_cover')) {
										$image = get_field('book_cover');
										if( !empty($image) ): 
											// vars
											$url = $image['url'];
											$title = $image['title'];
											// thumbnail
											$size = 'small-book';
											$thumb = $image['sizes'][ $size ];
											$width = $image['sizes'][ $size . '-width' ];
											$height = $image['sizes'][ $size . '-height' ];
										endif; ?>
										<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> book cover" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="cover" />
										<?php } else { ?>
											<div class="custom-cover cover">
												<span class="title"><?php the_title(); ?></span>
											</div>
										<?php } ?>
									<dl>
										<dt class="title">
											<?php the_title(); ?>
										</dt>
										<?php if(get_field('publisher')) { ?>
										<dd class="publisher">
											<?php the_field('publisher'); ?>, <?php the_field('published_date'); ?>
										</dd>
										<?php } ?>
									</dl>
								</li>
								<?php endforeach; ?>
								<?php wp_reset_postdata(); ?>
								<?php endif; ?>
							</ul>
						</section>
						<?php } ?>
						<?php if(get_field('articles')) { ?>
						<section id="articles">
							<h2>Articles</h2>
							<?php the_field('articles'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('book_introductions')) { ?>
						<section id="book_introductions">
							<h2>Book Introductions and Afterwords</h2>
							<?php the_field('book_introductions'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('encyclopedia_entries')) { ?>
						<section id="encyclopedia_entries">
							<h2>Encyclopedia Entries</h2>
							<?php the_field('encyclopedia_entries'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('reviews')) { ?>
						<section id="reviews">
							<h2>Reviews</h2>
							<?php the_field('reviews'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('translations')) { ?>
						<section id="translations">
							<h2>Translations</h2>
							<?php the_field('translations'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('custom_section_title')) { ?>
						<section id="other">
							<h2><?php the_field('custom_section_title'); ?></h2>
							<?php the_field('custom_section_content'); ?>
						</section>
						<?php } ?>
					</article>
					<?php endwhile; ?>
					<?php else : endif; ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
<?php get_footer(); ?>
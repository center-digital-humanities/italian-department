<div class="col content-col <?php the_sub_field('content_width'); ?>">
	<h3><?php the_sub_field('content_title'); ?></h3>
	<?php the_sub_field('content'); ?>
	<?php if(get_sub_field('show_button') == "yes") { ?>
	<a class="view-all" href="<?php the_sub_field('button_link'); ?>">&raquo; <?php the_sub_field('button_text'); ?></a>
	<?php } ?>
</div>
<?php
/*
 Template Name: Staff Page
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_post_thumbnail('content-width'); ?>
							<?php the_content(); ?>
							<?php if( have_rows('staff_list_left') ): ?>
							<div class="left_column">
							<?php while( have_rows('staff_list_left') ): the_row(); ?>
								<h4><?php the_sub_field('group_title'); ?></h4>
								<?php if( have_rows('person') ): ?>
								<dl>
								<?php while( have_rows('person') ): the_row(); ?>
								<?php if( get_sub_field('name') ): ?>
									<dt><h6><?php the_sub_field('name'); ?></h6></dt>
								<?php endif; ?>
								<?php if( get_sub_field('position') ): ?>
									<dd><?php the_sub_field('position'); ?></dd>
								<?php endif; ?>
								<?php if( get_sub_field('phone_number') ): ?>
									<dd><strong>Phone</strong>: <a href="tel:<?php the_sub_field('phone_number'); ?>"><?php the_sub_field('phone_number'); ?></a></dd>
								<?php endif; ?>
								<?php if( get_sub_field('email_address') ): ?>
									<dd><a href="mailto:<?php the_sub_field('email_address'); ?>"><?php the_sub_field('email_address'); ?></a></dd>
								<?php endif; ?>
								<?php endwhile; ?>
								</dl>
								<?php endif; ?>
							<?php endwhile; ?>
							</div>
							<?php endif; ?>
							
							<?php if( have_rows('staff_list_right') ): ?>
							<div class="right_column">
							<?php while( have_rows('staff_list_right') ): the_row(); ?>
								<h4><?php the_sub_field('group_title'); ?></h4>
								<?php if( have_rows('person') ): ?>
								<dl>
								<?php while( have_rows('person') ): the_row(); ?>
								<?php if( get_sub_field('name') ): ?>
									<dt><h6><?php the_sub_field('name'); ?></h6></dt>
								<?php endif; ?>
								<?php if( get_sub_field('position') ): ?>
									<dd><?php the_sub_field('position'); ?></dd>
								<?php endif; ?>
								<?php if( get_sub_field('phone_number') ): ?>
									<dd><strong>Phone</strong>: <a href="tel:<?php the_sub_field('phone_number'); ?>"><?php the_sub_field('phone_number'); ?></a></dd>
								<?php endif; ?>
								<?php if( get_sub_field('email_address') ): ?>
									<dd><a href="mailto:<?php the_sub_field('email_address'); ?>"><?php the_sub_field('email_address'); ?></a></dd>
								<?php endif; ?>
								<?php endwhile; ?>
								</dl>
								<?php endif; ?>
							<?php endwhile; ?>
							</div>
							<?php endif; ?>
														
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>
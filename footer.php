		<footer role="contentinfo">
			<div class="content">
				<?php get_search_form(); ?>
				<nav role="navigation" aria-label="Footer Navigation">
					<?php 
					// If there is a menu set for the footer use that
					if ( has_nav_menu( 'footer-nav' ) ) {
						wp_nav_menu(array(
							'container' => '',
							'menu' => __( 'Footer Menu', 'bonestheme' ),
							'menu_class' => 'footer-nav',
							'theme_location' => 'footer-nav',
							'before' => '',
							'after' => '',
							'depth' => 1,
						));
					} else {
					// If not, use the main menu
						wp_nav_menu(array(
							'container' => '',
							'menu' => __( 'Main Menu', 'bonestheme' ),
							'menu_class' => 'footer-nav',
							'theme_location' => 'main-nav',
							'before' => '',
							'after' => '',
							'depth' => 1,
						));
					} ?>
					<?php if(get_field('facebook', 'option') || get_field('twitter', 'option') || get_field('instagram', 'option') || get_field('contact_us', 'option')) { ?>
						<ul class="social-links">
						<?php if(get_field('facebook', 'option')) { ?>
							<li class="icon"><a href="<?php the_field('facebook', 'option'); ?>"><span class="fab fa-facebook" aria-label="Like us on Facebook"></span></a></li>
						<?php } if(get_field('twitter', 'option')) { ?>
							<li class="icon"><a href="<?php the_field('twitter', 'option'); ?>"><span class="fab fa-twitter" aria-label="Follow us on Twitter"></span></a></li>
						<?php } if(get_field('instagram', 'option')) { ?>
							<li class="icon"><a href="<?php the_field('instagram', 'option'); ?>"><span class="fab fa-instagram" aria-label="Follow us on Instagram"></span></a></li>
						<?php } if(get_field('contact_us', 'option')) { ?>
							<li class="icon"><a href="<?php the_field('contact_us', 'option'); ?>"><span class="fas fa-envelope" aria-label="Contact us"></span></a></li>
						<?php } ?>
						</ul>
					<?php } ?>
				</nav>
				<div class="copyright">
					<p class="vcard">
						<?php if (get_field('department_name', 'option')) { ?>	
						<?php the_field('department_name', 'option'); ?> is part of the <a href="http://humanities.ucla.edu">Humanities Division</a> within <a href="http://www.college.ucla.edu/">UCLA College</a>.<br />
						<?php } ?>
						<span class="adr">
							<?php if (get_field('main_office_location', 'option')) { ?>	
							<span class="street-address"><?php the_field('main_office_location', 'option'); ?></span> <span class="divider">|</span> <span class="locality"><?php the_field('city_state', 'option'); ?></span> <span class="postal-code"><?php the_field('zip_code', 'option'); ?></span>
							<?php } ?>
							<?php if (get_field('phone_number', 'option')) { ?>	
							<span class="divider">|</span> <span class="tel"><strong>P:</strong> <span class="value"><?php the_field('phone_number', 'option'); ?></span></span>
							<?php } ?>
							<?php if (get_field('fax_number', 'option')) { ?>	
							<span class="divider">|</span> <span class="fax"><strong>F:</strong> <?php the_field('fax_number', 'option'); ?>
							<?php } ?>
							<?php if (get_field('dept_email_address', 'option')) {
							$department_email = antispambot(get_field('dept_email_address', 'option')); ?>
							<span class="divider">|</span> <span class="email"><strong>E:</strong> <a href="mailto:<?php echo $department_email; ?>"><?php echo $department_email; ?></a></span>
							<?php } ?>
						</span>
						<br />
						University of California &copy; <?php echo date('Y'); ?> UC Regents
					</p>
					<?php if(get_field('facebook', 'option') || get_field('twitter', 'option') || get_field('instagram', 'option') || get_field('contact_us', 'option')) { ?>
						<nav role="navigation" aria-label="Social Navigation" class="mobile-social-nav">
							<ul class="social-links">
							<?php if(get_field('facebook', 'option')) { ?>
								<li class="icon"><a href="<?php the_field('facebook', 'option'); ?>"><span class="fab fa-facebook" aria-label="Like us on Facebook"></span></a></li>
							<?php } if(get_field('twitter', 'option')) { ?>
								<li class="icon"><a href="<?php the_field('twitter', 'option'); ?>"><span class="fab fa-twitter" aria-label="Follow us on Twitter"></span></a></li>
							<?php } if(get_field('instagram', 'option')) { ?>
								<li class="icon"><a href="<?php the_field('instagram', 'option'); ?>"><span class="fab fa-instagram" aria-label="Follow us on Instagram"></span></a></li>
							<?php } if(get_field('contact_us', 'option')) { ?>
								<li class="icon"><a href="<?php the_field('contact_us', 'option'); ?>"><span class="fas fa-envelope" aria-label="Contact us"></span></a></li>
							<?php } ?>
							</ul>
						</nav>
					<?php } ?>
					<a href="http://www.ucla.edu" class="university-logo"><img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo-white.svg" alt="UCLA" /></a>
				</div>
			</div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>